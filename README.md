![Logo-Final.png](https://image.noelshack.com/fichiers/2018/23/6/1528562794-logo-final.png)

## Steemit To Busy

Steemit To Busy (  abrégé STB ) est une extension  permettant de rediriger les urls de Steemit.com en urls de Busy.org.
Exemple : En chargeant la page https://busy.org/@wargof vous êtes automatiquement rediriger vers https://steemit.com/@wargof.

#### Infos

**Website :** https://sites.google.com/view/steemit-to-busy/accueil

**Développeur :** Wargof

**License :** GNU GPLv3

#### Fonctionnalités actuelles ou futures

- Redirection des urls steemit.com en urls busy.org après chargement d'une page sur steemit.com. ![Valid.png](https://image.noelshack.com/fichiers/2018/23/6/1528557538-valid-25px.png)

- Fenêtre dans la barre d'outils.  ![Valid.png](https://image.noelshack.com/fichiers/2018/23/6/1528557538-valid-25px.png)


- Activation ou désactivation depuis l'icone dans la barre d'outils. ![Croix.png](https://image.noelshack.com/fichiers/2018/23/6/1528557651-croix-25px.png)

- Transformer les urls Steemit en urls Busy dans les pages webs. ![Croix.png](https://image.noelshack.com/fichiers/2018/23/6/1528557651-croix-25px.png)

## Steemit To Busy version 0.1.1

#### Note de version :

- Ajout d'une fenêtre avec une icône cliquable dans la barre d'outils.
- Réorganisation du script principal ( ajout de fonctions et de conditions ).
- Chgangement des icônes.

## Steemit To Busy version 0.2.0