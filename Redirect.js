/*
Extension Firefox pour rediriger les urls Steemit en urls Busy tout en gardant la location de la page.
Créé par Wargof | https://steemit.com/@wargof https://steemit.com/@wargofosef https://cryptoblog.pw |
Version 0.1.1

### Fonctionnalités :
- Redirige les pages dont le domaine est steemit.com vers la même page sur le domaine busy.org

### Futures fonctionnalités
- Transformation des liens href dans les pages de steemit.com vers busy.org 

*/


// Déclaration de OnOff et attribution de 1
// Si OnOff = 1 alors l'extension est activé donc elle fonctionne
// Si OnOff = 0 alors l'extension est désactivé, elle ne doit rien faire 
var OnOff = 1; // On active l'extension par default

// Fonctions
function redirection() {
    // Redirection des urls ciblés ( steemit.com indiqué dans manifest.json ) vers les mêmes urls en Busy.org
    window.location.replace("https://busy.org" + window.location.pathname);
}

// Condition qui test la valeur de OnOff
if ( OnOff == 1 ) {
    redirection();
}
else if (OnOff == 0 ) {
    write("Extension désactivé.");
}
else {
    alert("Bug inconnu, résultat condition : else ");
}
